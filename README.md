Installation guide:
1. Use node version 10 (nvm use 10)
2. git clone this repo and npm install
3. Make sure you have .env file (for simplicity i have kept .env file in repo)
4. Run `npm index.js` or `nodemon index.js` server will run on port 8080
5. To run test cases run `npm test` (run all test cases in tests folder)

Hit below route to access data:
GET: http://localhost:8080/api/visitors?date=1404198000000&museum=avila_adobe
