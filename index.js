require('dotenv').config()

const express = require('express');
const app = express();

app.use(require('./routes'));

const server = app.listen(8080, () => console.log('Application running on port 8080!'))

module.exports = server;