const axios = require('axios');

const request = (requestObj) => {
  return axios({
    ...requestObj,
    headers: {
      'X-App-Token': process.env.APP_TOKEN
    }
  })
}

const getDate = (dateInMs) => {
  const parsedDate = new Date(parseInt(dateInMs, 10));
  return parsedDate.toISOString().split('T')[0];
}

const getMonth = date => {
  const parsedDate = new Date(date);
  return parsedDate.toLocaleString('en', { month: 'short' })
}

const getYear = date => {
  return new Date(date).getFullYear()
}

const isValidDate = date => {
  return date instanceof Date && !isNaN(date.valueOf());
}

module.exports = {
  request,
  getDate,
  getMonth,
  getYear,
  isValidDate
}