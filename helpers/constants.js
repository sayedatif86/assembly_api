const API_URL = 'https://data.lacity.org/resource/trxm-jn3c.json';
const MUSEUM_KEYS = [
  'avila_adobe',
  'chinese_american_museum',
  'firehouse_museum',
  'hellman_quon',
  'pico_house',
  'visitor_center_avila_adobe'
]

module.exports = {
  API_URL,
  MUSEUM_KEYS,
}