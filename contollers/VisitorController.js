const {
  request,
  getDate,
  getMonth,
  getYear,
  isValidDate,
} = require("../helpers");
const { MUSEUM_KEYS, API_URL } = require("../helpers/constants");

module.exports = {
  get: async (req, res) => {
    try {
      let { date, museum } = req.query;
      // check for query params
      if (!date || !museum || !MUSEUM_KEYS.includes(museum)) {
        throw new Error("invalid_request");
      }

      date = getDate(date);
      if (!isValidDate) {
        throw new Error("invalid_date");
      }

      // get request for museum visitor
      const { data } = await request({
        method: "GET",
        url: API_URL,
        params: {
          month: date,
        },
      });

      if (!data || !data.length) {
        return res.status(200).send({});
      }
      const dataToSend = {
        result: {
          month: getMonth(data[0].month),
          year: getYear(data[0].month),
          museum,
          visitors: parseInt(data[0][museum], 10),
        },
      };
      return res.status(200).send(dataToSend);
    } catch (err) {
      // console.error('err', err);
      return res.status(400).send({ message: "Something Failed", err: err.message });
    }
  },
};
