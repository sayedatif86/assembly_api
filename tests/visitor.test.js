const request = require('supertest')
const app = require('../index')

describe('Get Visitors', () => {
  it('Should fetch visitor for museum', async () => {
    const res = await request(app)
      .get('/api/visitors')
      .query({
        date: '1404198000000',
        museum: 'avila_adobe'
      })
    expect(res.statusCode).toBe(200);
    expect(res.body).toHaveProperty('result');
    expect(typeof res.body.result.month).toBe('string');
    expect(typeof res.body.result.museum).toBe('string');
    expect(typeof res.body.result.year).toBe('number');
    expect(typeof res.body.result.visitors).toBe('number');
  })

  it('Should return empty object', async () => {
    const res = await request(app)
      .get('/api/visitors')
      .query({
        date: '1615205313174',
        museum: 'avila_adobe'
      })
    expect(res.statusCode).toBe(200);
    expect(Object.keys(res.body).length).toBe(0);
  })

  it('Should fail if no date query', async () => {
    const res = await request(app)
      .get('/api/visitors')
      .query({
        museum: 'avila_adobe'
      })
    expect(res.statusCode).toBe(400);
  })

  it('Should fail if invalid museum name', async () => {
    const res = await request(app)
      .get('/api/visitors')
      .query({
        date: '1404198000000',
        museum: 'cat'
      })
    expect(res.statusCode).toBe(400);
  })
})

afterAll(() => {
  app.close();
})