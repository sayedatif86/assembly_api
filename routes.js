const express = require('express');
const router = express.Router();
const VisitorController = require('./contollers/VisitorController.js');

router.get('/api/visitors', VisitorController.get);

module.exports = router;